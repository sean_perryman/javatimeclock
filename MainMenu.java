import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.Date;

public class MainMenu extends JFrame {
    //Declare private class variables
    private final GridBagLayout gridBagLayout;
    private final GridBagConstraints gbc;
    private final JPanel upper, middle, lower;
    private final JLabel title, status;
    private final JButton addNewEmpBtn, punchInOutBtn, timeReportBtn, exitBtn;
    
    //Default constructor
    public MainMenu() {
        //JFrame settings
        super("Main Menu");
        setSize(800, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        //Panel and GBL/GBC Setup
        gridBagLayout = new GridBagLayout();
        gbc = new GridBagConstraints();
        upper = new JPanel(gridBagLayout);//Upper Section
        middle = new JPanel(gridBagLayout);//Middle Section
        lower = new JPanel(gridBagLayout);//Lower Section
                
        //Upper Section - Title
        title = new JLabel("Company Name - Time Clock");
        gbc.gridy = 0;
        gbc.insets = new Insets(20, 0, 20, 0);
        upper.add(title, gbc);
        
        //Middle Section - Buttons
        addNewEmpBtn = new JButton("Add New Employee");
        addNewEmpBtn.setPreferredSize( new Dimension( 150, 30));
        addNewEmpBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //move to add employee screen 
                TimeClockApp.ChangeScene("addEmployee");
            }
        });
        gbc.gridy = 2;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(addNewEmpBtn, gbc);
        
        punchInOutBtn = new JButton("Punch IN/OUT");
        punchInOutBtn.setPreferredSize( new Dimension( 150, 30));
        punchInOutBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //move to punch in/out screen
                TimeClockApp.ChangeScene("punchInOut");
            }
        });
        gbc.gridy = 3;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(punchInOutBtn, gbc);
        
        timeReportBtn = new JButton("Time Report");
        timeReportBtn.setPreferredSize( new Dimension( 150, 30));
        timeReportBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //move to time report screen
                TimeClockApp.ChangeScene("timeReport");
            }
        });
        gbc.gridy = 4;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(timeReportBtn, gbc);
        
        exitBtn = new JButton("Exit Application");
        exitBtn.setPreferredSize( new Dimension( 150, 30));
        exitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //exit program
                System.exit(0);
            }
        });
        gbc.gridy = 5;
        gbc.insets = new Insets(0, 0, 50, 0);
        middle.add(exitBtn, gbc);
        
        //Lower Section - Not implemented yet. Perhaps status text?
        status = new JLabel(new Date().toString());
        gbc.gridy = 6;
        gbc.insets = new Insets(0, 0, 10, 0);
        gbc.anchor = GridBagConstraints.SOUTH;
        lower.add(status, gbc);
        
        //Add panels to the JFrame
        add(upper, BorderLayout.NORTH);
        add(middle, BorderLayout.CENTER);
        add(lower, BorderLayout.SOUTH);
    }
}
