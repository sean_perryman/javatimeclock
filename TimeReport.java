
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class TimeReport extends JFrame {
    private final GridBagLayout gridBagLayout;
    private final GridBagConstraints gbc;
    private final JPanel upper, middle, lower;
    private final JLabel title, status;
    private final JTextField individualInput;
    private final JButton individualReportBtn, allEmployeesReportBtn, returnToMainMenuBtn;
    
    public void ToggleVisible(boolean visible) {
        this.setVisible(visible);
    }

    //Default constructor
    public TimeReport() {
        //JFrame settings
        super("Time Clock Report");
        setSize(800, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        //Panel and GBL/GBC Setup
        gridBagLayout = new GridBagLayout();
        gbc = new GridBagConstraints();
        upper = new JPanel(gridBagLayout);//Upper Section
        middle = new JPanel(gridBagLayout);//Middle Section
        lower = new JPanel(gridBagLayout);//Lower Section
                
        //Upper Section - Title
        title = new JLabel("Company Name - Time Clock Report");
        gbc.gridy = 0;
        gbc.insets = new Insets(20, 0, 0, 0);
        upper.add(title, gbc);
                
        //Middle Section
        individualReportBtn = new JButton("Individual");
        individualReportBtn.setPreferredSize( new Dimension( 100, 30));
        individualReportBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int employeeID = Integer.parseInt(individualInput.getText());
                boolean validEmployeeID = false;
                String output = "";

                for (Employee emp : TimeClockApp.employee) {
                    if (emp.GetEmployeeID() == employeeID) {
                        output += emp.GetEmployeeFirstName() + " ";
                        output += emp.GetEmployeeLastName() + " - Employee ID #";
                        output += emp.GetEmployeeID() + "\n\n";

                        for (TimeClock punch : emp.GetPunches()) {
                            output += punch.GetPunchDate() + " Direction: ";
                            if ("I".equals(punch.GetPunchDirection())) {
                                output += "In\n";
                            } else {
                                output += "Out\n";
                            }
                        }
                        validEmployeeID = true;
                    }
                }

                if (!validEmployeeID) {
                    TimeClockApp.infoBox("Please enter a valid employee ID.", "Error!");
                } else {
                    TimeClockApp.infoBox(output, "Individual Report");
                }
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, -50, 10, 0);
        
        middle.add(individualReportBtn, gbc);
        
        individualInput = new JTextField(3);
        individualInput.setText( "0" );
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, -35, 10, 0);
        middle.add(individualInput, gbc);

        allEmployeesReportBtn = new JButton("All Employees");
        allEmployeesReportBtn.setPreferredSize( new Dimension( 150, 30));
        allEmployeesReportBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String output = "";
                for (Employee emp : TimeClockApp.employee) {
                    output += emp.GetEmployeeFirstName() + " ";
                    output += emp.GetEmployeeLastName() + " - Employee ID #";
                    output += emp.GetEmployeeID() + "\n\n";
                    
                    for (TimeClock punch : emp.GetPunches()) {
                        output += punch.GetPunchDate() + " Direction: ";
                        if ("I".equals(punch.GetPunchDirection())) {
                            output += "In\n";
                        } else {
                            output += "Out\n";
                        }
                                
                    }
                    output += "\n";
                }
                TimeClockApp.infoBox(output, "All Employees Report");
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(allEmployeesReportBtn, gbc);
        
        returnToMainMenuBtn = new JButton("Return to Main Menu");
        returnToMainMenuBtn.setPreferredSize( new Dimension( 150, 30));
        returnToMainMenuBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Return to Main Menu
                TimeClockApp.ChangeScene("mainMenu");
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(returnToMainMenuBtn, gbc);
        
        //Lower Section - Not implemented yet. Perhaps status text?
        status = new JLabel(new Date().toString());
        gbc.gridy = 6;
        gbc.insets = new Insets(0, 0, 10, 0);
        gbc.anchor = GridBagConstraints.SOUTH;
        lower.add(status, gbc);
        
        //Add panels to the JFrame
        add(upper, BorderLayout.NORTH);
        add(middle, BorderLayout.CENTER);
        add(lower, BorderLayout.SOUTH);
    }
}
