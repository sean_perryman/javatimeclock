import java.util.ArrayList;
import java.util.Date;

public class Employee {
    //Declare variables
    private final int EmployeeID;
    private final String FirstName;
    private final String LastName;
    private ArrayList<TimeClock> punches;
        
    public Employee(String lastName, String firstName) {
        this.FirstName = firstName;
        this.LastName = lastName;
        this.punches = new ArrayList<>();
        this.EmployeeID = TimeClockApp.employee.size();
    }
    
    public int GetEmployeeID() {
        return this.EmployeeID;
    }
    
    public String GetEmployeeFirstName() {
        return this.FirstName;
    }
    
    public String GetEmployeeLastName() {
        return this.LastName;
    }
    
    public ArrayList<TimeClock> GetPunches() {
        return this.punches;
    }
    
    public TimeClock GetLastPunchDirection() {
        if (this.punches.size() > 0) {
            return this.punches.get(punches.size() - 1);
        } else {
            return null;
        }
    }
    
    public void PunchTimeClock(String direction) {
        Date date = new Date();
        this.punches.add(new TimeClock(this.EmployeeID, direction, new Date()));
        
        String directionIndicator = "";
        if (direction == "I") { directionIndicator = " In "; }
        else if (direction == "O") { directionIndicator = " Out "; }
        
        //Display popup telling user they are successfully punched in
        TimeClockApp.infoBox("Employee ID: " + this.EmployeeID + "\nTime Punch: " + date.toString(), "Punched" + directionIndicator + "Successfully!");
    }
    
    public void PunchTimeClock(Date date) {
        this.punches.add(new TimeClock(this.EmployeeID, "I", date));
        
        //Display popup telling user they are successfully punched in
        TimeClockApp.infoBox("Employee ID: " + this.EmployeeID + "\nTime Punch: " + date.toString(), "Punched In Successfully!");
    }
}
