import java.util.Date;

public class TimeClock {
    private final int EmployeeID;
    private final String PunchDirection;
    private final Date PunchDate;
    
    public TimeClock (int employeeID, String punchDirection, Date punchDate) {
        this.EmployeeID = employeeID;
        this.PunchDirection = punchDirection;
        this.PunchDate = punchDate;
    }
    
    public String GetPunchDirection() {
        return this.PunchDirection;
    }
    
    public int GetEmployeeID() {
        return this.EmployeeID;
    }
    
    public Date GetPunchDate() {
        return this.PunchDate;
    }
}
