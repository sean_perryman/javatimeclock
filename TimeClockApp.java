import java.util.ArrayList;
import javax.swing.JOptionPane;

public class TimeClockApp {
    //Declare variables
    public static AddEmployee addEmployee;
    public static PunchInOut punchInOut;
    public static TimeReport timeReport;
    public static MainMenu mainMenu;
    
    //String variable to hold what screen the application is in
    private static String applicationStatus;
    
    //Public storage variables
    public static ArrayList<Employee> employee;
    
    public static void main(String[] args) {
        //Set up new employee list
        employee = new ArrayList<>();
        
        //Pre-add employees for testing
        employee.add(new Employee("Brown", "Cleveland")); 
        employee.add(new Employee("Tyler", "Rose")); 
        employee.add(new Employee("Belcher", "Bob")); 
        
        //Instantiate other screen classes
        addEmployee = new AddEmployee();
        addEmployee.setLocationRelativeTo(null);
        punchInOut = new PunchInOut();
        punchInOut.setLocationRelativeTo(null);
        timeReport = new TimeReport();
        timeReport.setLocationRelativeTo(null);
        mainMenu = new MainMenu();
        mainMenu.setLocationRelativeTo(null);
        
        ChangeScene("mainMenu");
    }
    
    public static void infoBox(String infoMessage, String location) {
        JOptionPane.showMessageDialog(null, infoMessage, "InfoBox: " + location, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void ChangeScene(String scene) {
        applicationStatus = scene;
        
        switch (applicationStatus) {
            case "mainMenu":
                mainMenu.setVisible(true);
                addEmployee.setVisible(false);
                punchInOut.setVisible(false);
                timeReport.setVisible(false);
                break;
            case "addEmployee":
                mainMenu.setVisible(false);
                addEmployee.setVisible(true);
                punchInOut.setVisible(false);
                timeReport.setVisible(false);                
                break;
            case "punchInOut":
                mainMenu.setVisible(false);
                addEmployee.setVisible(false);
                punchInOut.setVisible(true);
                timeReport.setVisible(false);
                break;
            case "timeReport":
                mainMenu.setVisible(false);
                addEmployee.setVisible(false);
                punchInOut.setVisible(false);
                timeReport.setVisible(true);
                break;
            default:
                break;
        }
    }
}
