import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.Date;

public class AddEmployee extends JFrame {
    private final GridBagLayout gridBagLayout;
    private final GridBagConstraints gbc;
    private final JPanel upper, middle, lower;
    private final JLabel title, status, firstName, lastName;
    private final JTextField firstNameInput, lastNameInput;
    private final JButton addNewEmpBtn, returnToMainMenuBtn;
    
    public void ToggleVisible(boolean visible) {
        this.setVisible(visible);
    }

    //Default constructor
    public AddEmployee() {
        //JFrame settings
        super("Add Employee Screen");
        setSize(800, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        //Panel and GBL/GBC Setup
        gridBagLayout = new GridBagLayout();
        gbc = new GridBagConstraints();
        upper = new JPanel(gridBagLayout);//Upper Section
        middle = new JPanel(gridBagLayout);//Middle Section
        lower = new JPanel(gridBagLayout);//Lower Section
                
        //Upper Section - Title
        title = new JLabel("Company Name - Add Employee");
        gbc.gridy = 0;
        gbc.insets = new Insets(20, 0, 20, 0);
        upper.add(title, gbc);
                
        //Middle Section
        lastName = new JLabel("Last Name");
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, 75, 10, 165);
        middle.add(lastName, gbc);
        lastNameInput = new JTextField(7);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, 75, 10, 0);
        middle.add(lastNameInput, gbc);
        
        firstName = new JLabel("First Name");
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.insets = new Insets(0, 75, 10, 165);
        middle.add(firstName, gbc);
        firstNameInput = new JTextField(7);
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.insets = new Insets(0, 75, 10, 0);
        middle.add(firstNameInput, gbc);
        
        addNewEmpBtn = new JButton("Add New Employee");
        addNewEmpBtn.setPreferredSize( new Dimension( 150, 30));
        addNewEmpBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Pull text fields into temporary variables
                String newLastName = lastNameInput.getText();
                String newFirstName = firstNameInput.getText();
                Boolean employeeAlreadyExists;
                int employeeAlreadyExistsID = 0;
                
                //Test to make sure both fields are filled out
                if (newLastName.length() > 0 && newFirstName.length() > 0) {
                     employeeAlreadyExists = false;
                    
                    for (int i = 0; i < TimeClockApp.employee.size(); i++) {
                        Employee employee = TimeClockApp.employee.get(i);
                        
                        System.out.println("Employee record number " + i);
                        
                        if (newLastName.equals(employee.GetEmployeeLastName()) &&
                            newFirstName.equals(employee.GetEmployeeFirstName()))
                        {
                            employeeAlreadyExists = true;
                            employeeAlreadyExistsID = i;
                            break;
                        }
                    }

                    //If employee doesn't exist, add it. If it does, error out.
                    if (!employeeAlreadyExists) {
                        //Add employee to list
                        TimeClockApp.employee.add(new Employee(newLastName, newFirstName));

                        //Retrieve employee number from list
                        int t_lastEmployeeNumber = TimeClockApp.employee.size() - 1;

                        //Pop up alert displaying added user data and employee number
                        TimeClockApp.infoBox("First Name: " + newFirstName + 
                                            "\nLast Name: " + newLastName +
                                            "\nEmployee ID: " + t_lastEmployeeNumber, "New Employee Added!");

                        //Empty out text fields
                        lastNameInput.setText("");
                        firstNameInput.setText("");

                        //Return to main menu
                        TimeClockApp.ChangeScene("mainMenu");
                    } else {
                        TimeClockApp.infoBox("Employee already exists.\nEmployee ID: " + employeeAlreadyExistsID, "Error!");
                        lastNameInput.setText("");
                        firstNameInput.setText("");
                    }
                } else {
                    TimeClockApp.infoBox("All fields must be filled out.", "Error!");
                }
            }
        });
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(addNewEmpBtn, gbc);
        
        returnToMainMenuBtn = new JButton("Return to Main Menu");
        returnToMainMenuBtn.setPreferredSize( new Dimension( 150, 30));
        returnToMainMenuBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Return to Main Menu
                TimeClockApp.ChangeScene("mainMenu");
            }
        });
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(returnToMainMenuBtn, gbc);
        
        //Lower Section - Not implemented yet. Perhaps status text?
        status = new JLabel(new Date().toString());
        gbc.gridy = 6;
        gbc.insets = new Insets(0, 0, 10, 0);
        gbc.anchor = GridBagConstraints.SOUTH;
        lower.add(status, gbc);
        
        //Add panels to the JFrame
        add(upper, BorderLayout.NORTH);
        add(middle, BorderLayout.CENTER);
        add(lower, BorderLayout.SOUTH);
    }
}
