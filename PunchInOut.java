import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class PunchInOut extends JFrame {
    private final GridBagLayout gridBagLayout;
    private final GridBagConstraints gbc;
    private final JPanel upper, middle, lower;
    private final JLabel title, status, employeeID;
    private final JTextField employeeIDInput;
    private final JButton punchInBtn, punchOutBtn, findEmpIDBtn, returnToMainMenuBtn;
    
    /**
     * Default constructor
     */
    public PunchInOut() {
        //JFrame settings
        super("Punch In/Out Screen");
        setSize(800, 600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        //Panel and GBL/GBC Setup
        gridBagLayout = new GridBagLayout();
        gbc = new GridBagConstraints();
        upper = new JPanel(gridBagLayout);//Upper Section
        middle = new JPanel(gridBagLayout);//Middle Section
        lower = new JPanel(gridBagLayout);//Lower Section
                
        //Upper Section - Title
        title = new JLabel("Company Name - Punch In/Out Screen");
        gbc.gridy = 0;
        gbc.insets = new Insets(20, 0, 20, 0);
        upper.add(title, gbc);
                
        //Middle Section - Employee ID Input
        employeeID = new JLabel("Employee ID");
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, 0, 10, 20);
        middle.add(employeeID, gbc);
        
        employeeIDInput = new JTextField(4);
        employeeIDInput.setText("0");
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(employeeIDInput, gbc);
          
        // Punch in button
        punchInBtn = new JButton("Punch In");
        punchInBtn.setPreferredSize( new Dimension( 150, 30));
        punchInBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Pull text fields into temporary variables
                String tempEmployeeID = employeeIDInput.getText(); //Declare and initialize entered employee ID temp variable                    
                if (!tempEmployeeID.equals("")) {
                    int activeEmployeeID = Integer.valueOf(tempEmployeeID); //Cast entered employee ID to int
                    PunchClock("I", activeEmployeeID);
                } else {
                    TimeClockApp.infoBox( "Please enter an employee ID\nor click Find Employee ID.", "Error!" );
                }
            }
        });
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.insets = new Insets(0, 0, 10, 20);
        middle.add(punchInBtn, gbc);
        
        // Punch out button
        punchOutBtn = new JButton("Punch Out");
        punchOutBtn.setPreferredSize( new Dimension( 150, 30));
        punchOutBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Pull text fields into temporary variables
                String tempEmployeeID = employeeIDInput.getText(); //Declare and initialize entered employee ID temp variable
                if (!tempEmployeeID.equals("")) {
                    int activeEmployeeID = Integer.valueOf(tempEmployeeID); //Cast entered employee ID to int
                    PunchClock("O", activeEmployeeID);
                } else {
                    TimeClockApp.infoBox( "Please enter an employee ID\nor click Find Employee ID.", "Error!" );
                }
            }
        });
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(punchOutBtn, gbc);
        
        // Find employee button
        findEmpIDBtn = new JButton("Find EmployeeID");
        findEmpIDBtn.setPreferredSize( new Dimension( 150, 30));
        findEmpIDBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Boolean employeeFound = false;
                String searchString = JOptionPane.showInputDialog(null, "Enter employee first or last name", "Find Employee ID", 1);
                if (!searchString.equals("")) {
                    for (Employee emp : TimeClockApp.employee) {
                        if (searchString.equals(emp.GetEmployeeFirstName()) ||
                            searchString.equals(emp.GetEmployeeLastName())) {
                            TimeClockApp.infoBox("Employee: " + emp.GetEmployeeFirstName() + " " + emp.GetEmployeeLastName() + "\nEmployee ID: " + emp.GetEmployeeID(), "Employee ID Found!");
                            employeeFound = true;
                            employeeIDInput.setText("" + emp.GetEmployeeID());
                            break;
                        }
                    }
                    if (!employeeFound) {
                        TimeClockApp.infoBox("No employee found with that information.", "No Employee Found");
                    }
                } else {
                    TimeClockApp.infoBox("Please fill in either employee first or last name.", "Error!");
                }
            }
        });
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.insets = new Insets(0, 0, 10, 20);
        middle.add(findEmpIDBtn, gbc);
        
        returnToMainMenuBtn = new JButton("Return to Main Menu");
        returnToMainMenuBtn.setPreferredSize( new Dimension( 150, 30));
        returnToMainMenuBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Return to Main Menu
                TimeClockApp.ChangeScene("mainMenu");
            }
        });
        gbc.gridx = 2;
        gbc.gridy = 3;
        gbc.insets = new Insets(0, 0, 10, 0);
        middle.add(returnToMainMenuBtn, gbc);
        
        //Lower Section - Not implemented yet. Perhaps status text?
        status = new JLabel(new Date().toString());
        gbc.gridy = 6;
        gbc.insets = new Insets(0, 0, 10, 0);
        gbc.anchor = GridBagConstraints.SOUTH;
        lower.add(status, gbc);
        
        //Add panels to the JFrame
        add(upper, BorderLayout.NORTH);
        add(middle, BorderLayout.CENTER);
        add(lower, BorderLayout.SOUTH);
    }
    
    public static Boolean CheckLastPunchDirection( Employee employee, String direction ) {
        TimeClock lastPunch = employee.GetLastPunchDirection();
        if (lastPunch != null) {
            return !lastPunch.GetPunchDirection().equals(direction);
        } else if (lastPunch == null && direction.equals("I")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Punches the time clock with the specified direction (in/out) and employee ID
     * @param direction
     * @param activeEmployeeID
     */
    public static void PunchClock(String direction, int activeEmployeeID) {
        Employee employee = null; //Declare employee variable
        Boolean verifiedEmployeeID = false; //Bool to establish whether employeeID is valid or not
       
        //Test to make sure both fields are filled out
        if (activeEmployeeID >= 0) {

            for (int i = 0; i < TimeClockApp.employee.size(); i++) {
                employee = TimeClockApp.employee.get(i);

                //Test each entry in the employee list for the correct employee ID
                if (activeEmployeeID == employee.GetEmployeeID()) {
                    verifiedEmployeeID = true; //Set verified employeeID var true
                    break; //Break out of for loop
                }
            }
            if (!verifiedEmployeeID) { //False if employeeID was not found
                TimeClockApp.infoBox("Not a valid employee ID.", "Error!");
            } else {
                if (employee != null) {
                    if (CheckLastPunchDirection(employee, direction)) {
                        employee.PunchTimeClock(direction);
                    } else {
                        //Prompt user for clock in time if punching out
                        Date missedPunch = PromptClockInTime();
                        if (missedPunch != null) {
                            employee.PunchTimeClock(missedPunch);
                            
                            //Then punch clock the direction they were going before.
                            employee.PunchTimeClock(direction);
                        }
                    }
                }
            }
        } else {
            TimeClockApp.infoBox("All fields must be filled out.", "Error!");
        }
    }
    
    /**
     * Takes hour and minute text input from user and formats it before
     * converting to a date object for return.
     */
    public static Date PromptClockInTime() {
        Date date = null;
        String hour, minute;
        
        Calendar cal = Calendar.getInstance();
        
        int dayNumber = cal.get(Calendar.DAY_OF_MONTH);;
        String dayName = new SimpleDateFormat("EEE").format(dayNumber);
        int yearNumber = cal.get(Calendar.YEAR); //Year
        String monthName = new SimpleDateFormat("MMM").format(cal.getTime()); //Current Month Name
        String timeZone = "PST";
        
        //Prompt user to enter time in HH:mm:ss format
        hour = JOptionPane.showInputDialog(null, "Enter Punch In Hour(0-23)", "Punch In Hour", 1);
        if(hour != null) {
            minute = JOptionPane.showInputDialog(null, "Enter Punch In Minute(0-59)", "Punch In Minute", 1);
            if (minute != null) {
                String output = dayName + " " + dayNumber + " " + monthName + " " + hour + ":" + minute + ":00 " + timeZone + " " + yearNumber;
                try {
                    date = new SimpleDateFormat("EEE d MMM HH:mm:ss z y").parse(output);
                } catch (ParseException ex) {
                    Logger.getLogger(PunchInOut.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            TimeClockApp.infoBox("Try again.", "Error");
        }
        
        return date;
    }
}
